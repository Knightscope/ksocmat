import time
from datetime import datetime    
import pytest
import os

from selenium import webdriver as selenium_webdriver
from selenium.webdriver.chrome.options import Options
global admNum
global chrome_driver
import test_MAT

def pytest_sessionstart(session):
    # setup_stuff
    print("pytest_sessionstart starting")
    setupChrome(True)

def pytest_sessionfinish(session):
    print("That's a wrap!")
    chrome_driver.close()
    
def setupChrome(headless):
    options = selenium_webdriver.ChromeOptions()
    #options.add_argument("no-sandbox")
    #options.add_argument("--window-size=800,600")
    if headless:
        print("setupChrome: headless = True")
        options.headless = True
    else:
        print("setupChrome: headless = False")
        options.headless = False

    global chrome_driver
    chrome_driver = selenium_webdriver.Chrome(options=options)
    print("setupChrome:Starting up chrome")


#Fixture for Chrome
@pytest.fixture(scope="class", autouse=True)
def chrome_driver_init(request):
    if request is None:
        return
    if request.cls is None:
        return
    global chrome_driver
    print("chrome driver set")
    chrome_driver.implicitly_wait(10)
    chrome_driver.maximize_window()
    request.cls.driver = chrome_driver
    request.cls.admNum = test_MAT.admNum
    #yield
    #chrome_driver.close()

# set up a hook to be able to check if a test has failed
@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"

    setattr(item, "rep_" + rep.when, rep)

# check if a test has failed
@pytest.fixture(scope="function", autouse=True)
def test_failed_check(request):
    yield
    # request.node is an "item" because we use the default
    # "function" scope
    if request.node.rep_setup.failed:
        print("setting up a test failed!", request.node.nodeid)
    elif request.node.rep_setup.passed:
        if request.node.rep_call.failed:
#           driver = request.node.funcargs['selenium_driver']
            take_screenshot(chrome_driver, request.node.nodeid)
            print("executing test failed", request.node.nodeid)

# make a screenshot with a name of the test, date and time
def take_screenshot(driver, nodeid):
    time.sleep(1)
    file_name = f'{nodeid}_{datetime.today().strftime("%Y-%m-%d_%H:%M")}.png'.replace("/","_").replace("::","__")
    #driver.save_screenshot(file_name)

# Handle input arguments;
# admNum - What is the ADM Number being tested
# Env    - Which environment is being tested; prod, staging, qa, dev
#
def pytest_addoption(parser):
    print('conftest method')
    parser.addoption(
        "--admNum", action="store", default="12", help="Adm number under test"
    )
    parser.addoption(
        "--env", action="store", default="prod", help="Environment to test under"
    )
    parser.addoption(
        "--headless", action="store", default="True", help="Headless?"
    )
   
@pytest.fixture(scope='session')
def getAdmNum(request):
    admNum = request.config.getoption("--admNum")
    if admNum is None:
        pytest.skip()
    print("getAdmNum returning ", admNum)
    return admNum

@pytest.fixture(scope='session')
def getEnv(request):
    env = request.config.getoption("--env")
    if env is None:
        pytest.skip()
    print("getEnv request = ", env)
    return env
    
@pytest.fixture(scope='session')
def getHeadless(request):
    hl = request.config.getoption("--headless")
    if hl is None:
        return True
    print("getHeadless:headless request = ", hl)
    return hl
    
def test_print_admNum(getAdmNum):
        print(f"\ncommand line param (getAdmNum): {getAdmNum}")

def test_print_getEnv(getEnv):
    print(f"\ncommand line param (getEnv): {getEnv}")

def test_print_headless(getHeadless):
    print(f"\ncommand line param (getHeadless): {getHeadless}")
    
def test_print_admNum(browser):
    print(f"\ncommand line param (browser): {browser}")
    
@pytest.fixture
def get_param(request):
    config_param = {}
    config_param["AdmNum"] = request.config.getoption("--admNum")
    config_param["Env"] = request.config.getoption("--env")
    config_param["Browser"] = request.config.getoption("--browser")
    return config_param
