from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
import pytest
import pytest_html
from time import sleep

import sys
import conftest
#import tests.test_Machine
 
from tests.test_Machine import *
#import test_Machine

admNum = "12"
baseUrl = "https://www.ksoc.co"
env = "prod"
#driver = webdriver.Chrome()

def test_admNum(getAdmNum):
    newAdmNum = getAdmNum
    global admNum
    admNum = newAdmNum
    print("admNum = ", admNum)

def test_env(getEnv):
    global env, baseUrl
    env = getEnv
    print("Env under test = ", env)
    if env == "prod":
        print("Using prod: Default.")
    elif env == "staging":
        print("Using staging")
        baseUrl = "https://staging.ksoc.co/"
    elif env == "dev2":
        print("Using Dev2")
        baseUrl = "https://ksocv66-dev2.knightscope.internal/" 
    elif env == "dev3":
        print("Using Dev3")
        baseUrl = "https://ksocv66-dev3.knightscope.internal/"        
    elif env == "poc":
        print("Using Poc")
        baseUrl = "https://ksoc-poc-02.knightscope.internal/"        
    else:
        print("Unrecognized env = ", env, "- Defaulting to prod")
        env = "prod"
    print("env = ", env, ", baseurl = ", baseUrl)

def test_headless(getHeadless):
    headless = getHeadless
    print("test_headless: headless = ", headless)
    if headless == "False":
        print("test_headless:Killing chrome")
        conftest.chrome_driver.quit()
        conftest.setupChrome(False)
        print("test_headless:Restarting Chrome")

@pytest.mark.skip
def test_MAT():
    testNum = 1
    machine = TestMachine()
    #machine.setup_class(admNum)
    machine.init(admNum)
    if testNum <= 1:
        matched = machine.test_1stADM(test_Machine.navToMachineURL)
        print('Test ', testNum, ' result = ', matched)
        assert matched
        testNum += 1

    if testNum <= 2:
        matched = machine.test_CamView()
        print('Test ', testNum, ' result. Machine ', admNum, ' cam view matched = ', matched)
        assert matched
        testNum += 1

    if testNum <= 3:
        matched = machine.test_AdmState()
        print('Test ', testNum, ' result. Machine ', admNum, ' matched = ', matched)
        assert matched
        testNum += 1

    if testNum <= 4:
        found = machine.test_AdmPatrolRoute()
        print("test #", testNum, ", result = ", found)
        assert(found)
        testNum += 1
    
        machine.tearDown()

if __name__ == "__main__":

#    runAll()
    assert 0