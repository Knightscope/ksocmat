import pytest


def pytest_addoption(parser):
    print('conftest method')
    parser.addoption(
        "--admNum", action="store", default="12", help="Adm number under test"
    )
    parser.addoption(
        "--env", action="store", default="prod", help="which environment are we targeting"
    )
    parser.addoption(
        "--browser", action="store", default="chrome", help="which browser are we using"
    )
   
@pytest.fixture
def get_param(request):
    config_param = {}
    config_param["AdmNum"] = request.config.getoption("--admNum")
    config_param["Env"] = request.config.getoption("--env")
    config_param["Browser"] = request.config.getoption("--browser")
    return config_param

@pytest.fixture
def getAdmNum(request):
    return request.config.getoption("--admNum")

def getEnv(pytestconfig):
    return pytestconfig.getoption("env")

def browser(pytestconfig):
    return pytestconfig.getoption("browser")

def test_print_admNum(admNum):
    print(f"\ncommand line param (admNum): {admNum}")

def test_print_admNum(env):
    print(f"\ncommand line param (env): {env}")

def test_print_admNum(browser):
    print(f"\ncommand line param (browser): {browser}")