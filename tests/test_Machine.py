import json
import os
import re
from time import sleep
from _pytest.outcomes import skip
from datetime import datetime, timedelta


import pytest
from selenium import webdriver
from selenium.webdriver import chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import conftest
import test_MAT

# Class globals
machineNumLocator = "machine-num"
containsMin = '//*[contains(text(), "MIN")]'
topStatus = '/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/div[1]/div[1]/h3'
detailsStatus = '//*[@id="machine-details-info"]/p[2]'
machineZone = 'machine-zone'
textContent = 'textContent'
machineUrl = "https://www.ksoc.co/KHQ/machines/"

@pytest.fixture
def navToMachineURL():
    destUrl = test_MAT.baseUrl + "/KHQ/machines/"
    #driver = webdriver.Chrome(options=chrome_options)
    driver = conftest.chrome_driver
    driver.get(destUrl)
    currentURL = driver.current_url
    assert(currentURL == destUrl)
    el = WebDriverWait(driver, timeout=5).until(lambda d: d.find_element_by_tag_name("p"))
    return 'text-content'

@pytest.mark.usefixtures("chrome_driver_init")
class Basic_Chrome_Test:
    pass

#
# Class to process all of the Machine MAT tests -> https://knightscope.atlassian.net/wiki/spaces/KD/pages/1861943419/ADM+Bumblebee+-+KSOC+MAT
#
class TestMachine(Basic_Chrome_Test):

    # Set some variables to be used later
    # Basic initialization
    #
    def init(self, admNum):
#        self.driver.implicitly_wait(15)
        print("init:adm = ", admNum)
        self.admNum = admNum
        self.driver = conftest.chrome_driver
        result = self.test_login()
        #test_login(self.driver)
        destUrl = test_MAT.baseUrl + "/KHQ/machines/"
        self.driver.get(destUrl)
        currentURL = self.driver.current_url
        assert(currentURL == destUrl)
        el = WebDriverWait(self.driver, timeout=5).until(lambda d: d.find_element_by_tag_name("p"))
#        print('text =' + el.text + '.')
#        assert "Logged in!" in el.text

    # 
    # Login to ksoc.co/KHQ, or other URLs, depending on env passed on cmd line
    #
    def test_login(self):
        self.admNum = test_MAT.admNum
        #driver = webdriver.Chrome(options=chrome_options)
        driver = conftest.chrome_driver
        url = test_MAT.baseUrl + "/KHQ/"
        print("test_login: url = ", url)
        driver.get(url)
        #assert(homeUrl == url)
        element = driver.find_element_by_id("id_uname")
        element.send_keys("auto")
        element = driver.find_element_by_id("id_pword")
        element.send_keys("auto1234")
        element = driver.find_element_by_id("submit").click()
        homeUrl = driver.current_url
        print("homeUrl = ", homeUrl, ", url = ", url)
        assert(homeUrl == url)
        print("Logged in!")

    # Navigate to the machine URL by clicking the machine icon
    #
    def test_navToMachines(self):
        driver = conftest.chrome_driver
        try:
            # //img[@alt='machines']
            # //*[@id="right-nav-bar-text"]
            # //div[@id='right-nav-bar-item-machines']
            # //*[@id="right-nav-bar"]
            machinePage = driver.find_element(By.XPATH,"//img[@alt='machines']")
            #machinePage = driver.find_element(By.ID,"right-nav-bar")
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("test_navTomachines: " + message)
            assert(0)
        machinePage.click()
        sleep(3)
        currUrl = driver.current_url
        print("currUrl = ", currUrl)
        assert(currUrl == machineUrl)

    # Verify the ADM under test shows up
    #
    def test_1stADM(self):
        print("Verify1stADM. expected Adm = ", self.admNum)
        print("machineNumLocator = ", machineNumLocator)
        driver = conftest.chrome_driver
        # Get list of all adms on the page and 
        machines = driver.find_elements(By.ID, machineNumLocator)
#        assert(machines)
        for machine in machines:
            print("machine = ", machine, ", text = ", machine.text, machine.get_attribute(textContent))
        machineIdId = machines[0].get_attribute(textContent)
        print("machine id = ", machines[0].text)
        print("machine id (text) = ", machineIdId)
        assert machineIdId == self.admNum
        return machineIdId == self.admNum
    
    #
    # Verify and validate on the Cam view, 
    # by default the first machine is selected and 
    # live video should be started from the selected machine
    #
    @pytest.mark.mat
    @pytest.mark.skip
    def test_CamView(self):
        print("VerifyCamView. Adm Num = ", self.admNum)
        #driver = selenium_webdriver.Chrome(options=chrome_options)
        driver = conftest.chrome_driver
        machineNum = driver.find_element(By.XPATH, containsMin)
        machineNumText = machineNum.get_attribute(textContent)
        print("machineNum = ", machineNumText)
#       num = [int(s) for s in machineNum.text.split() if s.isdigit()]
        num = re.findall(r'\d+', machineNumText)
        print("found num = ", num)
        print("num[0]=", num[0])
        # Need to parse string - 'MIN12 - K5'
        assert(num[0] == self.admNum)
        return num[0] == self.admNum
    
    #
    # Verify that the ADM state matches between the video section
    # And, the Machine ID section
    #
    @pytest.mark.mat
    @pytest.mark.skip
    def test_AdmState(self):
        print("Testing VerifyAdmState. admNum = ", self.admNum)
        #driver = webdriver.Chrome(options=chrome_options)
        driver = conftest.chrome_driver
        sleep(2)
        #topStateEl =self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div[2]/div[2]/div/div[2]/div[1]/div[1]/h3")
        topStateEl = driver.find_element(By.XPATH, topStatus)
        print('top state attributes = ', topStateEl.get_attribute(textContent))
        topStatusText = topStateEl.get_attribute(textContent)

        detailsStateEl = driver.find_element(By.XPATH, detailsStatus)
        detailsStatusText = detailsStateEl.get_attribute(textContent)
        print('details state name = ', detailsStateEl, ", text = ", detailsStatusText)
        print('details text = ', detailsStateEl.text)
        #assert topState == detailsState
        return topStatusText == detailsStatusText

    #
    # Verify that the ADM under test's patrol path matches
    #
    @pytest.mark.mat
    @pytest.mark.skip
    def test_AdmPatrolRoute(self):
        print("Testing verifyAdmPatrolRoute: admNum=", self.admNum)
        driver = conftest.chrome_driver
        patrolPath = driver.find_element(By.ID, machineZone)
        patrolPathText = patrolPath.get_attribute(textContent)
        print("path = ", patrolPath.text)
        print("path attribute = ", patrolPathText)
        return 'path'.upper() in patrolPathText or 'zone'.upper() in patrolPathText

    # Select the 2nd Adm on the page
    @pytest.mark.mat
    @pytest.mark.skip
    def test_2ndADM(self):
        print("verify2ndAdm = ", self.admNum)
        #driver = webdriver.Chrome(options=chrome_options)
        driver = conftest.chrome_driver
        machines = driver.find_elements(By.ID, machineNumLocator)
        for machine in machines:
            print("machine = ", machine, ", text = ", machine.text)
        assert(machines)
        print("machine id = ", machines[1].text)
        assert machines[1].text == self.admNum
        return machines[1].text == self.admNum

    #
    # Verify and validate that clicking the Front, Left, Rear, and Right buttons 
    # below the video should toggle between the respective cameras
    #
    @pytest.mark.mat
    @pytest.mark.skip
    def test_FrontCamView(self):
        print("verifyFrontCam", self.admNum)
        driver = conftest.chrome_driver
        #`<span title="Click to view the front camera" class="active">Front</span>
        camLabel = driver.find_element(By.CLASS_NAME, 'camera-position')
        videoLabel = driver.find_element(By.XPATH, '//*[@id="front-video-container-overlay"]/div[1]')
        print('camlabel = ', camLabel.text, ", videoLabel = ", videoLabel.text)
        assert(camLabel.text == videoLabel.text)

    #
    # Verify and validate that clicking the Front, Left, Rear, and Right buttons 
    # below the video should toggle between the respective cameras
    #
    @pytest.mark.mat
    @pytest.mark.skip
    def test_LeftCamView(self):
        print("verifyLeftCam", self.admNum)
        driver = conftest.chrome_driver
        videoLabel = driver.find_element(By.XPATH, "//span[@title='Click to view the left camera']")
        videoLabel.click()
        sleep(3) #give camera a chance to switch over
        # Now, verify that the labels match
        camLabel = driver.find_element(By.CLASS_NAME, 'camera-position')
        # //*[@id="select-cam-container"]/span[2]
        videoLabel = driver.find_element(By.XPATH, "//span[@title='Click to view the left camera']")
        print('camlabel = ', camLabel.text, ", videoLabel = ", videoLabel.text)
        assert(videoLabel.text in camLabel.text)

    #
    # Verify and validate that clicking the Front, Left, Rear, and Right buttons 
    # below the video should toggle between the respective cameras
    #
    @pytest.mark.mat
    @pytest.mark.skip
    def test_RightCamView(self):
        print("verifyLeftCam", self.admNum)
        driver = conftest.chrome_driver
        videoLabel = driver.find_element(By.XPATH, "//span[@title='Click to view the right camera']")
        videoLabel.click()
        sleep(3) #give camera a chance to switch over
        # Now, verify that the labels match
        camLabel = driver.find_element(By.CLASS_NAME, 'camera-position')
        videoLabel = driver.find_element(By.XPATH, "//span[@title='Click to view the right camera']")
        print('camlabel = ', camLabel.text, ", videoLabel = ", videoLabel.text)
        assert(videoLabel.text in camLabel.text)

    #
    # Verify and validate that clicking the Front, Left, Rear, and Right buttons 
    # below the video should toggle between the respective cameras
    #
    @pytest.mark.mat
    @pytest.mark.skip
    def test_RearCamView(self):
        print("verifyLeftCam", self.admNum)
        driver = conftest.chrome_driver
        videoLabel = driver.find_element(By.XPATH, "//span[@title='Click to view the rear camera']")
        videoLabel.click()
        sleep(3) #give camera a chance to switch over
        # Now, verify that the labels match
        camLabel = driver.find_element(By.CLASS_NAME, 'camera-position')
        videoLabel = driver.find_element(By.XPATH, "//span[@title='Click to view the rear camera']")
        print('camlabel = ', camLabel.text, ", videoLabel = ", videoLabel.text)
        assert(videoLabel.text in camLabel.text)

    #
    # Verify and validate that clicking the Multiple button on the top right of the video feed 
    # should display all four camera feeds simultaneously
    # 
    @pytest.mark.mat
    @pytest.mark.skip
    def test_MultipleCamView(self):
        print("verifyLeftCam", self.admNum)
        driver = conftest.chrome_driver
        # //*[@id="machines-modal-video-header-icons"]/div[1]/p# 
        #videoLabel = driver.find_element(By.XPATH, "//span[@title='Click to view the rear camera']")
        videoLabel = driver.find_element(By.XPATH, '//*[@id="multi-video-icon"]')
        videoLabel.click()
        sleep(3) #give camera a chance to switch over
        # Now, verify that the labels match
        #camLabel = driver.find_element(By.CLASS_NAME, 'camera-position')
        # //*[@id="front-video-container-overlay"]/div[1]
        # Front - Live
        # Left - Live
        # Right - Live
        # Rear - Live
#            frontLabel = driver.find_element(By.PARTIAL_LINK_TEXT, "Front - Live")
        try:
            cameraLabels = driver.find_elements(By.CLASS_NAME, 'camera-position')
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print (message)
        frontLabel = cameraLabels[0]
        leftLabel = cameraLabels[1]
        rightLabel = cameraLabels[2]
        rearLabel = cameraLabels[3]
        print('frontLabel = ', frontLabel.text)
        print('leftLabel = ', leftLabel.text)
        print('rightLabel = ', rightLabel.text)
        print('rearLabel = ', rearLabel.text)
        assert("Live" in frontLabel.text)
        assert("Live" in leftLabel.text)
        assert("Live" in rightLabel.text)
        assert("Live" in rearLabel.text)

    #
    # Verify that the max view displays at max size
    #
    @pytest.mark.skip
    def test_maxView(self):
        driver = conftest.chrome_driver
        # expand icon
        try:
            expandView = driver.find_element(By.CLASS_NAME, "fullscreen-icon").click()
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("maxView: " + message)
        # See if we can see the fullscreenvideo = true flag
        isExpandedTrue = ""
        try:
            isExpandedTrue = driver.find_element(By.XPATH, "//div[@isfullscreenvideo='true']")
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("maxView: " + message)
            assert(0)
        print("maxView = ", isExpandedTrue)
        assert(1)

    #
    # Verify that the min view displays at max size
    #
    @pytest.mark.skip
    def test_minView(self):
        driver = conftest.chrome_driver
        # expand icon
        try:
            expandView = driver.find_element(By.CLASS_NAME, "fullscreen-icon").click()
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("minView: " + message)
        # See if we can see the fullscreenvideo = true flag
        isExpandedFalse = ""
        try:
            isExpandedFalse = driver.find_element(By.XPATH, "//div[@isfullscreenvideo='false']")
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("minView: " + message)
            assert(0)
        print("minView = ", isExpandedFalse)
        assert(1)

    #
    # Pause the live video
    # Verify and validate that clicking pause, waiting, then resume play will continue playing the video stream
    #
    @pytest.mark.skip
    def test_pauseVideo(self):
        driver = conftest.chrome_driver
        try:
            # //img[@id='pause-icon']
            pauseView = driver.find_element(By.ID, "pause-icon")
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("pauseVideo.pauseView: " + message)
            assert(0)

        pauseView.click()
        try:
            # //img[@id='pause-icon']
            playView = driver.find_element(By.ID, "play-icon-ctrl")
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("pauseVideo.playView: " + message)
            assert(0)

        assert(1)
        
    #
    # Verify and validate that clicking pause, waiting, then resume play will continue playing the video stream
    #
    @pytest.mark.skip
    def test_playVideo(self):
        driver = conftest.chrome_driver
        try:
            playView = driver.find_element(By.ID, "play-icon-ctrl")
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("pauseVideo.playView: " + message)
            assert(0)

        playView.click()
        try:
            # //img[@id='pause-icon']
            pauseView = driver.find_element(By.ID, "pause-icon")
        except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("pauseVideo.pauseView: " + message)
            assert(0)

        assert(1)
  
    # Verify and validate a recorded video can be fetched for playback at any time within the past 14 days
    def test_pick14days(self): 
       driver = conftest.chrome_driver
       try:
           datePicker = driver.find_element(By.ID, 'timeline-datepicker')
       except Exception as ex:
           template = "An exception of type {0} occurred. Arguments:\n{1!r}"
           message = template.format(type(ex).__name__, ex.args)
           print ("pick14Days.datePicker: " + message)
           assert(0)

       datePicker.click()
       daysAgo14 = datetime.today() - timedelta(days=14)
       fullDayStr = daysAgo14.strftime('%a %m/%d/%Y')
       dayStr = daysAgo14.strftime('%d')
       print("fulldayStr = ", fullDayStr)

       # wait for the check in input to load
       #wait = WebDriverWait(driver, 10) 

       try:
            days = driver.find_elements(By.CLASS_NAME, 'react-datepicker__day')
       except Exception as ex:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            print ("pick14Days: pick days: " + message)
            assert(0)

       for day in days:
            if day.text != dayStr:
                #print("Not the right day ", day)
                continue
#
            print("day 14 = ", day.text)
            try:
                day.click()
            except Exception as ex:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print ("pick14Days: pick day: " + message)
                assert(0)

            #
            # Verify that we are at the right date
            #
            try:
                #xpathStr = "//span[normalize-space()='Fri 04/16/2021']")
                xpathStr = "//span[normalize-space()='"+ fullDayStr + "']"
                print("xpathStr = ", xpathStr)
                archivedDate = driver.find_element(By.XPATH, xpathStr)
            except:
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                print ("pick14Days: pick day: " + message)
                assert(0)
            
            if dayStr in archivedDate.text:
                assert(1)
            else:
                assert(0)
            break
       #
       # Verify and validate that clicking Skip to End (button control) i
       # should play the livestream video from the machine 
       #
       def test_skipToEnd(self):
          assert(1)

       # 
       # [Video Clips] Verify and validate that user can create video clips between 1-5 minutes long
       #
       def test_createVideoClip(self):
           assert(1)

       #
       # [Video Clips] Verify and validate that user can watch any successfully requested video clip on KSOC
       #
       def test_watchVideoClip(self):
           assert(1)

       #
       # [Video Clips] Verify and validate that user can download and watch any successfully requested video clip
       #
       def test_downloadClip(self):
           assert(1)

       # 
       # Verify and validate that when a machine is selected, 
       # the machine info should include MIN, 
       # machine type (or client-given name), state, and assigned patrol path (and/or zone)
       # 
       def test_ADMInfo(self):
           assert(1)

       #
       # Verify and validate that selecting a different machine from the right-side machine list 
       # should change the live video stream to the correct machine
       #
       def test_otherADM(self):
           assert(1)

       #
       #  Verify and validate that the machine responds correctly 
       # when clicking any/all of the machine controls (Emergency Stop / Park / Patrol)
       #
       def test_ADMControls(self):
           assert(1)
       
       # ALERTS
       #
       # Verify and validate that clicking the alarm button will cause the 
       # machine to broadcast an alarm sound at max volume for 5 seconds, 
       # while enabling the alarm LEDs
       #
       # Not sure that this can be automated
       def test_ADMAlarm(self):
           assert(1)

       #
       # Verify and validate that toggling the Patrol Sound button will broadcast 
       # a patrol sound from the machine, or turn it off (respectively)
       #
       def test_ADMPatrolSound(self):
           assert(1)

       #
       # Verify and validate that live audio can be heard from the selected machine
       #
       def test_liveAudio(self):
           assert(1)

       #
       # Verify and validate that pre-recorded messages can be broadcasted to the selected machine(s)
       #
       def test_preRecordedMsgs(self):
           assert(1)
       
       